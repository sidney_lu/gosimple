//服务端解包过程
package main

import (
	//"./protocol"
	"fmt"
	"net"
	"os"
	"es/elog"
	"bytes"
	"encoding/binary"
	//"netpb"
	//proto"code.google.com/p/goprotobuf/proto"
	"reflect"
)

func main() {
	netListen, err := net.Listen("tcp", ":9988")
	CheckError(err)

	defer netListen.Close()

	Log("Waiting for clients")
	for {
		conn, err := netListen.Accept()
		if err != nil {
			continue
		}

		Log(conn.RemoteAddr().String(), " tcp connect success")
		go handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) {
	//声明一个临时缓冲区，用来存储被截断的数据
	//tmpBuffer := make([]byte, 0)

	//管道,goroutines,声明一个管道用于接收解包的数据
	readerChannel := make(chan []byte, 16)
	go reader(readerChannel,conn)//协程

	buffer := make([]byte, 1024)
	for {
		n, err := conn.Read(buffer)
		
		if err != nil {
			Log(conn.RemoteAddr().String(), " connection error: ", err)
			return
		}
		elog.LogSys("handleConnection...")
		//tmpBuffer = protocol.Unpack(append(tmpBuffer, buffer[:n]...), readerChannel)
		readerChannel <- buffer[:n]
	}
}
//整形转换成字节
func IntToBytes(n int) []byte {
	x := int32(n)

	bytesBuffer := bytes.NewBuffer([]byte{})
	binary.Write(bytesBuffer, binary.BigEndian, x)
	return bytesBuffer.Bytes()
}

func reader(readerChannel chan []byte,conn net.Conn) {
	for {
		elog.LogSys("for read chan")
		select {
		case data := <-readerChannel:
			//Log(string(data))
			elog.LogSys(string(data))
			//write
			// msg := &netpb.SCLoginRsp{
	  //     		M_Ret : proto.Int64(5),
	  //     	}
	  //     	buffer, _  := proto.Marshal(msg)
   //      	elog.LogSys(string(buffer))
			// conn.Write(buffer)
			InvokeObjectMethod(new(YourT2), "MethodFoo", 10, "abc")
		}
	}
}
func InvokeObjectMethod(object interface{}, methodName string, args ...interface{}) {
    inputs := make([]reflect.Value, len(args))
    for i, _ := range args {
        inputs[i] = reflect.ValueOf(args[i])
    }
    reflect.ValueOf(object).MethodByName(methodName).Call(inputs)
}

type YourT2 struct {
}

func (y *YourT2) MethodFoo(i int, oo string) {
   elog.LogSys("MethodFoo")
}

// funcs := map[string]interface{}{"w1":w1, "bar":bar}

// func Call(m map[string]interface{}, name string, params ... interface{}) (result []reflect.Value, err error) {
//     var f = reflect.ValueOf(m[name])
//     if len(params) != f.Type().NumIn() {
//         err = errors.New("The number of params is not adapted.")
//         return
//     }
//     in := make([]reflect.Value, len(params))
//     for k, param := range params {
//         in[k] = reflect.ValueOf(param)
//     }
//     result = f[name].Call(in)
//     return
// }
// // Call(funcs, "foo")
// // Call(funcs, "bar", 1, 2, 3)

// func bar() {
	
// }
func w1() {
	elog.LogSys("w1")
}

func Log(v ...interface{}) {
	fmt.Println(v...)
}

func CheckError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}
