//客户端发送封包
package main

import (
	"./protocol"
	"fmt"
	"net"
	"os"
	//"time"
	"bufio"
    //s "strings"
    "netpb"
    proto"code.google.com/p/goprotobuf/proto"
    "es/elog"
)

func sender(conn net.Conn) {
	for i := 0; i < 1000; i++ {
		words := "{\"Id\":1,\"Name\":\"golang\",\"Message\":\"message\"}"
		conn.Write(protocol.Packet([]byte(words)))
	}
	fmt.Println("send over")
}

func main() {
	server := "127.0.0.1:9988"
	tcpAddr, err := net.ResolveTCPAddr("tcp4", server)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}

	defer conn.Close()
	fmt.Println("connect success")


	go receiver(conn)
  	for {
          reader := bufio.NewReader(os.Stdin)
	      str, err := reader.ReadString('\n')
	      fmt.Println(str,err)
	      if err != nil {
	   //       conn.Close()
	        break
	      }

	    //var strs= s.Split(str," ")
	    //words := "{\"Id\":" + strs[0] + ",\"Name\":\"" + strs[1] + "\",\"Message\":\"message\"}"

	    // msg := &netpb.NetMsg{ 

	    // }
	    // msg.M_ID = new( netpb.MsgID  )
     //    *msg.M_ID = netpb.MsgID_EM_CS_LOGIN
     //    msg.m_Name = "abc"
     //    msg.m_Pwd = "123"
	      msg := &netpb.CSLoginReq{
	      	M_Name : proto.String("abc"),
	      	M_Pwd : proto.String("1234"),
	      }
        buffer, _  := proto.Marshal(msg)
     	elog.LogSys(string(buffer))
		//conn.Write(protocol.Packet([]byte(words)))
		//conn.Write(protocol.Packet(buffer))
		conn.Write(buffer)
		

      }
	// go sender(conn)
	// for {
	// 	time.Sleep(1 * 1e9)
	// }
}

func receiver(conn net.Conn) {

	//readerChannel := make(chan []byte, 16)
	buffer := make([]byte, 1024)
	for {
		_, err := conn.Read(buffer)
		elog.LogSys("client read chan")
		if err!=nil {
			fmt.Println(err)
		}
		// select {
		// case data := <-readerChannel:
		// 	elog.LogSys(string(data))
		// }
		// 进行解码
  		newTest := &netpb.SCLoginRsp{}
  		err = proto.Unmarshal(buffer, newTest)
  		if newTest.GetM_Ret()!=20{
			elog.LogSys("%d",newTest.GetM_Ret())
		}
	}
}
